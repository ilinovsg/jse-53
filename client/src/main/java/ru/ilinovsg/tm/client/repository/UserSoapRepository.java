package ru.ilinovsg.tm.client.repository;

import ru.ilinovsg.tm.client.model.User;

import java.util.List;
import java.util.Optional;

public interface UserSoapRepository {
    Integer createUser(User user);

    Integer updateUser(User user);

    User findById(Long id);

    void delete(Long id);

    List<User> findAll();
}
