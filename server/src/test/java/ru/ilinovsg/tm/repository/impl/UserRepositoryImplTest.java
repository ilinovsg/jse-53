package ru.ilinovsg.tm.repository.impl;

import liquibase.Contexts;
import liquibase.LabelExpression;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import ru.ilinovsg.tm.entity.Project;
import ru.ilinovsg.tm.entity.User;

import java.sql.Connection;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;

class UserRepositoryImplTest {

    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String FIRSTNAME = "firstname";
    private static final String LASTNAME = "lastname";
    private static final String FIRSTNAME2 = "firstname2";
    private static final String LASTNAME2 = "lastname2";

    @BeforeAll
    static void setUp() {
        Connection connection = ConnectionHelper.getH2Connection();
        try {
            Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(connection));
            Liquibase liquibase = new Liquibase("databasechangelog.xml", new ClassLoaderResourceAccessor(), database);
            liquibase.update(new Contexts(), new LabelExpression());
        } catch (LiquibaseException e) {
            e.printStackTrace();
        }
    }

    @Test
    void create() {
        User user = User.builder().login(LOGIN).password(PASSWORD).firstName(FIRSTNAME).lastName(LASTNAME).build();
        UserRepositoryImpl userRepository = new UserRepositoryImpl();
        Optional<User> result = userRepository.create(user);
        if (result.isPresent()) {
            assertEquals(LOGIN, userRepository.findById(result.get().getId()).get().getLogin());
            assertEquals(PASSWORD, userRepository.findById(result.get().getId()).get().getPassword());
            assertEquals(FIRSTNAME, userRepository.findById(result.get().getId()).get().getFirstName());
            assertEquals(LASTNAME, userRepository.findById(result.get().getId()).get().getLastName());
        } else {
            throw new RuntimeException();
        }
    }

    @Test
    void update() {
        User user = User.builder().login(LOGIN).password(PASSWORD).firstName(FIRSTNAME).lastName(LASTNAME).build();
        UserRepositoryImpl userRepository = new UserRepositoryImpl();
        Optional<User> result = userRepository.create(user);
        user.setFirstName(FIRSTNAME2);
        user.setLastName(LASTNAME2);
        userRepository.update(user);
        if (result.isPresent()) {
            assertEquals(FIRSTNAME2, userRepository.findById(result.get().getId()).get().getFirstName());
            assertEquals(LASTNAME2, userRepository.findById(result.get().getId()).get().getLastName());
        } else {
            throw new RuntimeException();
        }
    }

    @Test
    void delete() {
        User user = User.builder().login(LOGIN).password(PASSWORD).firstName(FIRSTNAME).lastName(LASTNAME).build();
        UserRepositoryImpl userRepository = new UserRepositoryImpl();
        Optional<User> result = userRepository.create(user);
        if (result.isPresent()) {
            Long userId = result.get().getId();
            userRepository.delete(result.get().getId());
            assertFalse(userRepository.findById(userId).isPresent());
        } else {
            throw new RuntimeException();
        }
    }

    @Test
    void findAll() {
        UserRepositoryImpl userRepository = new UserRepositoryImpl();
        List<User> result = userRepository.findAll();
        assertEquals(12, result.size());
    }
}