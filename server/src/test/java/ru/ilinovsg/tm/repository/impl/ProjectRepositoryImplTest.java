package ru.ilinovsg.tm.repository.impl;

import liquibase.Contexts;
import liquibase.LabelExpression;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import ru.ilinovsg.tm.entity.Project;
import ru.ilinovsg.tm.entity.User;

import java.sql.Connection;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class ProjectRepositoryImplTest {

    private static final String NAME = "name";
    private static final String DESCRIPTION = "description";
    private static final String NAME2 = "name2";
    private static final String DESCRIPTION2 = "description2";

    @BeforeAll
    static void setUp() {
        Connection connection = ConnectionHelper.getH2Connection();
        try {
            Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(connection));
            Liquibase liquibase = new Liquibase("databasechangelog.xml", new ClassLoaderResourceAccessor(), database);
            liquibase.update(new Contexts(), new LabelExpression());
        } catch (LiquibaseException e) {
            e.printStackTrace();
        }
    }

    @Test
    void create() {
        Project project = Project.builder().name(NAME).description(DESCRIPTION).userId(1L).build();
        ProjectRepositoryImpl projectRepository = new ProjectRepositoryImpl();
        Optional<Project> result = projectRepository.create(project);
        if (result.isPresent()) {
            assertEquals(NAME, projectRepository.findById(result.get().getId()).get().getName());
            assertEquals(DESCRIPTION, projectRepository.findById(result.get().getId()).get().getDescription());
            assertEquals(1L, projectRepository.findById(result.get().getId()).get().getUserId());
        } else {
            throw new RuntimeException();
        }
    }

    @Test
    void update() {
        Project project = Project.builder().name(NAME).description(DESCRIPTION).userId(1L).build();
        ProjectRepositoryImpl projectRepository = new ProjectRepositoryImpl();
        Optional<Project> result = projectRepository.create(project);
        project.setName(NAME2);
        project.setDescription(DESCRIPTION2);
        project.setUserId(2L);
        projectRepository.update(project);
        if (result.isPresent()) {
            assertEquals(NAME2, projectRepository.findById(result.get().getId()).get().getName());
            assertEquals(DESCRIPTION2, projectRepository.findById(result.get().getId()).get().getDescription());
            assertEquals(2L, projectRepository.findById(result.get().getId()).get().getUserId());
        } else {
            throw new RuntimeException();
        }
    }

    @Test
    void delete() {
        Project project = Project.builder().name(NAME).description(DESCRIPTION).userId(1L).build();
        ProjectRepositoryImpl projectRepository = new ProjectRepositoryImpl();
        Optional<Project> result = projectRepository.create(project);
        if (result.isPresent()) {
            Long projectId = result.get().getId();
            projectRepository.delete(result.get().getId());
            assertFalse(projectRepository.findById(projectId).isPresent());
        } else {
            throw new RuntimeException();
        }
    }

    @Test
    void findAll() {
        ProjectRepositoryImpl projectRepository = new ProjectRepositoryImpl();
        List<Project> result = projectRepository.findAll();
        assertEquals(1, result.size());
    }
}