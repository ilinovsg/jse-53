package ru.ilinovsg.tm.repository.impl;

import liquibase.Contexts;
import liquibase.LabelExpression;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import ru.ilinovsg.tm.entity.Task;

import java.sql.Connection;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class TaskRepositoryImplTest {

    private static final String NAME = "name";
    private static final String DESCRIPTION = "description";
    private static final String NAME2 = "name2";
    private static final String DESCRIPTION2 = "description2";

    @BeforeAll
    static void setUp() {
        Connection connection = ConnectionHelper.getH2Connection();
        try {
            Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(connection));
            Liquibase liquibase = new Liquibase("databasechangelog.xml", new ClassLoaderResourceAccessor(), database);
            liquibase.update(new Contexts(), new LabelExpression());
        } catch (LiquibaseException e) {
            e.printStackTrace();
        }
    }

    @Test
    void create() {
        Task task = Task.builder().name(NAME).description(DESCRIPTION).userId(1L).projectId(1L).build();
        TaskRepositoryImpl taskRepository = new TaskRepositoryImpl();
        Optional<Task> result = taskRepository.create(task);
        if (result.isPresent()) {
            assertEquals(NAME, taskRepository.findById(result.get().getId()).get().getName());
            assertEquals(DESCRIPTION, taskRepository.findById(result.get().getId()).get().getDescription());
            assertEquals(1L, taskRepository.findById(result.get().getId()).get().getUserId());
            assertEquals(1L, taskRepository.findById(result.get().getId()).get().getProjectId());
        } else {
            throw new RuntimeException();
        }
    }

    @Test
    void update() {
        Task task = Task.builder().name(NAME).description(DESCRIPTION).userId(1L).projectId(1L).build();
        TaskRepositoryImpl taskRepository = new TaskRepositoryImpl();
        Optional<Task> result = taskRepository.create(task);
        task.setName(NAME2);
        task.setDescription(DESCRIPTION2);
        task.setUserId(2L);
        task.setProjectId(2L);
        taskRepository.update(task);
        if (result.isPresent()) {
            assertEquals(NAME2, taskRepository.findById(result.get().getId()).get().getName());
            assertEquals(DESCRIPTION2, taskRepository.findById(result.get().getId()).get().getDescription());
            assertEquals(2L, taskRepository.findById(result.get().getId()).get().getUserId());
            assertEquals(2L, taskRepository.findById(result.get().getId()).get().getProjectId());
        } else {
            throw new RuntimeException();
        }
    }

    @Test
    void delete() {
        Task task = Task.builder().name(NAME).description(DESCRIPTION).userId(1L).projectId(1L).build();
        TaskRepositoryImpl taskRepository = new TaskRepositoryImpl();
        Optional<Task> result = taskRepository.create(task);
        if (result.isPresent()) {
            Long taskId = result.get().getId();
            taskRepository.delete(result.get().getId());
            assertFalse(taskRepository.findById(taskId).isPresent());
        } else {
            throw new RuntimeException();
        }
    }

    @Test
    void findAll() {
        TaskRepositoryImpl taskRepository = new TaskRepositoryImpl();
        List<Task> result = taskRepository.findAll();
        assertEquals(12, result.size());
    }
}