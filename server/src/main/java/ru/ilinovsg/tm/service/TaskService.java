package ru.ilinovsg.tm.service;

import ru.ilinovsg.tm.dto.TaskDTO;
import ru.ilinovsg.tm.dto.TaskListResponseDTO;
import ru.ilinovsg.tm.dto.TaskResponseDTO;

public interface TaskService {
    TaskResponseDTO createTask(TaskDTO taskDTO);

    TaskResponseDTO updateTask(TaskDTO taskDTO);

    TaskResponseDTO deleteTask(Long id);

    TaskResponseDTO getTask(Long id);

    TaskListResponseDTO getAllTasks();

    TaskListResponseDTO findByName(String name);
}
