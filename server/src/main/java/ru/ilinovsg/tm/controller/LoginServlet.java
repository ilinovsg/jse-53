package ru.ilinovsg.tm.controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/loginservlet")
public class LoginServlet extends HttpServlet {
    private static final String USER_ID = "admin";
    private static final String USER_PASSWORD = "admin";

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String pwd = req.getParameter("password");

        if (USER_ID.equals(login) && USER_PASSWORD.equals(pwd)) {
            HttpSession session = req.getSession();
            session.setAttribute("login", login);
            resp.sendRedirect("loginSuccess.jsp");
        } else {
            RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/login.jsp");
            PrintWriter out = resp.getWriter();
            out.println("<font color=red> Login or password are incorrect</font>");
            requestDispatcher.include(req, resp);
        }
    }
}

