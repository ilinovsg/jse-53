package ru.ilinovsg.tm.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.ilinovsg.tm.annotation.MapTo;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Project extends AbstractEntity {
    @MapTo
    private Long id;
    @MapTo
    private String name = "";
    @MapTo
    private String description = "";
    @MapTo
    private Long userId;
}
