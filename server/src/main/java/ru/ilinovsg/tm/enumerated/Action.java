package ru.ilinovsg.tm.enumerated;

import java.util.HashMap;
import java.util.Map;

public enum Action {
    EXIT("exit"),
    CREATE_USER("createUser"),
    UPDATE_USER("updateUser"),
    DELETE_USER("deleteUser"),
    GET_USER("getUser"),
    GET_USER_BY_START("getUserByStart"),
    GET_USER_BY_NAME("getUserByName"),
    GET_ALL_USERS("getAllUsers"),
    EMPTY("");

    private final static Map<String, Action> map = new HashMap<>();

    static {
        for (Action action1 : Action.values()) {
            map.put(action1.getAction(), action1);
        }
    }

    private String action;

    Action(String action) {
        this.action = action;
    }

    public static Action findAction(String action) {
        return map.get(action);
    }

    public String getAction() {
        return action;
    }
}
