package ru.ilinovsg.tm.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.ilinovsg.tm.enumerated.Status;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProjectListResponseDTO {
    private Status status;
    private ProjectDTO[] payload;
}
