package ru.ilinovsg.tm.repository.impl;

import org.springframework.stereotype.Repository;
import ru.ilinovsg.tm.repository.ProjectRepository;
import ru.ilinovsg.tm.entity.Project;
import ru.ilinovsg.tm.repository.datasource.PooledDataSource;
import ru.ilinovsg.tm.repository.impl.mapper.ProjectMapper;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Repository
public class ProjectRepositoryImpl implements ProjectRepository {

    @Override
    public Optional<Project> create(Project input) {
        String sql = "insert into projects(name, description, userId) values (?, ?, ?)";
        try {
            DataSource dataSource = PooledDataSource.getDataSource();
            try (Connection connection = dataSource.getConnection();
                 PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
                ProjectMapper.setupStatement(preparedStatement, input);
                preparedStatement.executeUpdate();
                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    input.setId(resultSet.getLong(1));
                }
                return Optional.of(input);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }    return Optional.empty();
    }

    @Override
    public Optional<Project> update(Project input) {
        String sql = "update projects set name = ?, description = ?, userId = ? where id = ?";
        try {
            DataSource dataSource = PooledDataSource.getDataSource();
            try (Connection connection = dataSource.getConnection();
                 PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
                ProjectMapper.setupStatement(preparedStatement, input);
                preparedStatement.setLong(4, input.getId());
                preparedStatement.executeUpdate();
                return Optional.of(input);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Optional<Project> findById(Long id) {
        String sql = "select * from projects where id = ?";
        try {
            DataSource dataSource = PooledDataSource.getDataSource();
            try (Connection connection = dataSource.getConnection();
                 PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setLong(1, id);
                ResultSet resultSet = preparedStatement.executeQuery();
                if (resultSet.next()) {
                    return Optional.of(ProjectMapper.getProjectFromResultSet(resultSet));
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public void delete(Long id) {
        String sql = "delete from projects where id = ?";
        try {
            DataSource dataSource = PooledDataSource.getDataSource();
            try (Connection connection = dataSource.getConnection();
                 PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setLong(1, id);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public List<Project> findAll() {
        String sql = "select * from projects";
        try {
            DataSource dataSource = PooledDataSource.getDataSource();
            try (Connection connection = dataSource.getConnection();
                 Statement statement = connection.createStatement()) {
                ResultSet resultSet = statement.executeQuery(sql);
                List<Project> result = new ArrayList<>();
                while (resultSet.next()) {
                    result.add(ProjectMapper.getProjectFromResultSet(resultSet));
                }
                return result;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return Collections.emptyList();
    }
}
