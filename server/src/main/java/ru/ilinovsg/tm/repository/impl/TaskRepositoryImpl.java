package ru.ilinovsg.tm.repository.impl;

import org.springframework.stereotype.Repository;
import ru.ilinovsg.tm.repository.TaskRepository;
import ru.ilinovsg.tm.entity.Task;
import ru.ilinovsg.tm.repository.datasource.PooledDataSource;
import ru.ilinovsg.tm.repository.impl.mapper.TaskMapper;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Repository
public class TaskRepositoryImpl implements TaskRepository {

    @Override
    public Optional<Task> create(Task input) {
        String sql = "insert into tasks(name, description, userId, projectId) values (?, ?, ?, ?)";
        try {
            DataSource dataSource = PooledDataSource.getDataSource();
            try (Connection connection = dataSource.getConnection();
                 PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
                TaskMapper.setupStatement(preparedStatement, input);
                preparedStatement.executeUpdate();
                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    input.setId(resultSet.getLong(1));
                }
                return Optional.of(input);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Optional<Task> update(Task input) {
        String sql = "update tasks set name = ?, description = ?, userId = ?, projectId = ? where id = ?";
        try {
            DataSource dataSource = PooledDataSource.getDataSource();
            try (Connection connection = dataSource.getConnection();
                 PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
                TaskMapper.setupStatement(preparedStatement, input);
                preparedStatement.setLong(5, input.getId());
                preparedStatement.executeUpdate();
                return Optional.of(input);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Optional<Task> findById(Long id) {
        String sql = "select * from tasks where id = ?";
        try {
            DataSource dataSource = PooledDataSource.getDataSource();
            try (Connection connection = dataSource.getConnection();
                 PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setLong(1, id);
                ResultSet resultSet = preparedStatement.executeQuery();
                if (resultSet.next()) {
                    return Optional.of(TaskMapper.getTaskFromResultSet(resultSet));
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public void delete(Long id) {
        String sql = "delete from tasks where id = ?";
        try {
            DataSource dataSource = PooledDataSource.getDataSource();
            try (Connection connection = dataSource.getConnection();
                 PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setLong(1, id);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public List<Task> findAll() {
        String sql = "select * from tasks";
        try {
            DataSource dataSource = PooledDataSource.getDataSource();
            try (Connection connection = dataSource.getConnection();
                 Statement statement = connection.createStatement()) {
                ResultSet resultSet = statement.executeQuery(sql);
                List<Task> result = new ArrayList<>();
                while (resultSet.next()) {
                    result.add(TaskMapper.getTaskFromResultSet(resultSet));
                }
                return result;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return Collections.emptyList();
    }
}
