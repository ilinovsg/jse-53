package ru.ilinovsg.tm.repository.impl.mapper;

import ru.ilinovsg.tm.entity.Task;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TaskMapper {
    public static Task getTaskFromResultSet(ResultSet resultSet) throws SQLException {
        return Task.builder()
                .id(resultSet.getLong("id"))
                .name(resultSet.getString("name"))
                .description(resultSet.getString("description"))
                .userId(resultSet.getLong("userId"))
                .projectId(resultSet.getLong("projectId"))
                .build();
    }

    public static void setupStatement(PreparedStatement preparedStatement, Task input) throws SQLException {
        preparedStatement.setString(1, input.getName());
        preparedStatement.setString(2, input.getDescription());
        preparedStatement.setLong(3, input.getUserId());
        preparedStatement.setLong(4, input.getProjectId());
    }
}
