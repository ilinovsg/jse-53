package ru.ilinovsg.tm.repository.impl;

import org.springframework.stereotype.Repository;
import ru.ilinovsg.tm.repository.UserRepository;
import ru.ilinovsg.tm.entity.User;
import ru.ilinovsg.tm.repository.datasource.PooledDataSource;
import ru.ilinovsg.tm.repository.impl.mapper.UserMapper;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl implements UserRepository {

    @Override
    public Optional<User> create(User input) {
        String sql = "insert into users(login, password, firstName, lastName) values (?, ?, ?, ?)";
        try {
            DataSource dataSource = PooledDataSource.getDataSource();
            try (Connection connection = dataSource.getConnection();
                 PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
                UserMapper.setupStatement(preparedStatement, input);
                preparedStatement.executeUpdate();
                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    input.setId(resultSet.getLong(1));
                }
                return Optional.of(input);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Optional<User> update(User input) {
        String sql = "update users set login = ?, password = ?, firstName = ?, lastName = ? where id = ?";
        try {
            DataSource dataSource = PooledDataSource.getDataSource();
            try (Connection connection = dataSource.getConnection();
                 PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
                UserMapper.setupStatement(preparedStatement, input);
                preparedStatement.setLong(5, input.getId());
                preparedStatement.executeUpdate();
                return Optional.of(input);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Optional<User> findById(Long id) {
        String sql = "select * from users where id = ?";
        try {
            DataSource dataSource = PooledDataSource.getDataSource();
            try (Connection connection = dataSource.getConnection();
                 PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setLong(1, id);
                ResultSet resultSet = preparedStatement.executeQuery();
                if (resultSet.next()) {
                    return Optional.of(UserMapper.getUserFromResultSet(resultSet));
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public void delete(Long id) {
        String sql = "delete from users where id = ?";
        try {
            DataSource dataSource = PooledDataSource.getDataSource();
            try (Connection connection = dataSource.getConnection();
                 PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setLong(1, id);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public List<User> findAll() {
        String sql = "select * from users";
        try {
            DataSource dataSource = PooledDataSource.getDataSource();
            try (Connection connection = dataSource.getConnection();
                 Statement statement = connection.createStatement()) {
                ResultSet resultSet = statement.executeQuery(sql);
                List<User> result = new ArrayList<>();
                while (resultSet.next()) {
                    result.add(UserMapper.getUserFromResultSet(resultSet));
                }
                return result;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return Collections.emptyList();
    }
}
