package ru.ilinovsg.tm.repository;

import ru.ilinovsg.tm.entity.User;

public interface UserRepository extends Repository<User>{
}
