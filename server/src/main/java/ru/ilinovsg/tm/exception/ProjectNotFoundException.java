package ru.ilinovsg.tm.exception;

public class ProjectNotFoundException extends Exception{
    public ProjectNotFoundException(String message) {
        super(message);
    }
}
